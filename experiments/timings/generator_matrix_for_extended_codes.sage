### Imports
import time

### Define methods

def generator_matrix_from_pc_matrix(C):
    H = C.parity_check_matrix()
    G = H.right_kernel()
    return G.basis_matrix()

def generator_matrix_direct_computation(C):
    F = C.base_ring()
    Cor = C.original_code()
    G = Cor.generator_matrix()
    extra_col = [-sum(G.rows()[i]) for i in range(Cor.dimension())]
    extra_col = matrix(F, C.dimension(), 1,  extra_col)
    return G.augment(extra_col)

### Timings

n, k = 100, 50
timings_pc = []
timings_direct = []

for i in range(1, 11):
    tmp_pc = []
    tmp_direct = []
    for j in range(100):
        Cor = codes.RandomLinearCode(n * i, k * i, GF(1024, 'a'))
        C = ExtendedCode(Cor)
        C.parity_check_matrix()
        start = time.clock()
        generator_matrix_from_pc_matrix(C)
        elapsed = time.clock() - start
        tmp_pc.append(elapsed)
        start = time.clock()
        generator_matrix_direct_computation(C)
        elapsed = time.clock() - start
        tmp_direct.append(elapsed)
    timings_pc.append((n * i, median(tmp_pc)))
    timings_direct.append((n * i, median(tmp_direct)))
    print "Finished timings for length %s" % (n * i)

p1 = list_plot(timings_pc, plotjoined = True, color = 'red', legend_label = "PC matrix")
p2 = list_plot(timings_direct, plotjoined = True, color = 'green', legend_label = "Direct computation")
c = p1 + p2
c.save('plots/extended_code_direct_vs_pc_precomputed_pc_big_matrices.png', title = 'Gen mat for extended codes: PC matrix vs direct computation, precomputed PC matrix, nonprime field', legend_loc = 'upper right', axes_labels = ['code length', 'time (s)'])
print "Computation complete"
