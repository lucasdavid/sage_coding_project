### Imports
import time
from sage.plot.plot import list_plot

### Define methods

def dual_code_old(C):
    G = C.generator_matrix()
    H = G.transpose().kernel()
    V = H.ambient_vector_space()
    Cd = LinearCodeFromVectorSpace(V.span(H))
    return Cd

def dual_code_new(C):
    return LinearCode(C.parity_check_matrix())

### Timings

n, k = 10, 5
timings_old = []
timings_new = []
for i in range(1, 11):
    tmp_old = []
    tmp_new = []
    for j in range(100):
        C = codes.RandomLinearCode(n * i , k * i, GF(31))
        start = time.clock()
        dual_code_old(C)
        elapsed = time.clock() - start
        tmp_old.append(elapsed)
        start = time.clock()
        dual_code_new(C)
        elapsed = time.clock() - start
        tmp_new.append(elapsed)
    timings_old.append((n * i, median(tmp_old)))
    timings_new.append((n * i, median(tmp_new)))
    print "Finished timings for length %s " % (n * i)

p1 = list_plot(timings_old, plotjoined=True, color = 'red', legend_label = 'Old dual code')
p2 = list_plot(timings_new, plotjoined=True, color = 'green', legend_label = 'New dual code')
c = p1 + p2
c.save('plots/dual_code.png', title = 'old dual code vs new', legend_loc="upper right", axes_labels=['code length', 'time (s)'])
print "Computation complete"
###
