### Imports
import time
from sage.plot.plot import list_plot

### Define method

def equal_old(C1, C2):
        if not isinstance(C2, LinearCode):
            return False
        slength = C1.length()
        rlength = C2.length()
        sdim = C1.dimension()
        rdim = C2.dimension()
        sF = C1.base_ring()
        rF = C2.base_ring()
        if slength != rlength:
            return False
        if sdim != rdim:
            return False
        if sF != rF:
            return False
        sbasis = C1.gens()
        rbasis = C2.gens()
        scheck = C1.parity_check_matrix()
        rcheck = C2.parity_check_matrix()
        for c in sbasis:
            if rcheck*c:
                return False
        for c in rbasis:
            if scheck*c:
                return False
        return True

def equal_new(C1, C2):
    if not (isinstance(C2, LinearCode)\
            and C1.length() == C2.length()\
            and C1.dimension() == C2.dimension()\
            and C1.base_ring() == C2.base_ring()):
        return False
    Ks, rbas = C1.parity_check_matrix().C2_kernel(), C2.gens()
    if not all(c in Ks for c in rbas):
        return False
    Kr, sbas = C1.parity_check_matrix().C2_kernel(), C2.gens()
    if not all(c in Kr for c in sbas):
        return False
    return True

### Timings

n, k = 10, 5
timings_old = []
timings_new = []
for i in range(1, 11):
    tmp_old = []
    tmp_new = []
    for j in range(100):
        C1 = codes.RandomLinearCode(n * i , k * i, GF(31))
        C2 = codes.RandomLinearCode((n - 1) * i , k * i, GF(31))
        start = time.clock()
        equal_old(C1, C2)
        elapsed = time.clock() - start
        tmp_old.append(elapsed)
        start = time.clock()
        equal_new(C1, C2)
        elapsed = time.clock() - start
        tmp_new.append(elapsed)
    timings_old.append((n * i, median(tmp_old)))
    timings_new.append((n * i, median(tmp_new)))
    print "Finished timings for length %s " % (n * i)

p1 = list_plot(timings_old, plotjoined=True, color = 'red', legend_label = 'Old dual code')
p2 = list_plot(timings_new, plotjoined=True, color = 'green', legend_label = 'New dual code')
c = p1 + p2
c.save('plots/eq_trivial_ne.png', title = 'old __eq__ vs new', legend_loc="upper right", axes_labels=['code length', 'time (s)'])
print "Computation complete"
###
