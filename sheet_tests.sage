import sage.repl.preparse as preparser
import traceback

class TestFailed(Exception):
    def __init__(self, location):
        #TODO: The following line assumes that no exception was thrown between
        #the caught exception from the failed test and this exception
        self.tb = traceback.format_exc()
        self.location = location

class MyFileHandle(object):
    "File handle which supports line numbers"

    def __init__(self, filename, mode):
        self.handle = open(filename, mode)
        self.line = 0
        self.EOF = False

    def close(self):
        return self.handle.close()

    def readline(self):
        self.line += 1
        l = self.handle.readline()
        if l == "":
            self.EOF = True
        return l

    def name(self):
        return self.handle.name

def blockDelimiter(line):
    """Check if line is a block delimiter. If so, return the title."""
    if line[:3] == "###":
        return line[3:-1]
    else:
        return False

def isBlockDelimiter(line):
    "To avoid pitfall if using blockDelimiter in if-statement and an empty title is returned and checked for"
    return blockDelimiter(line) != False


def directive(line):
    if line[:2] == '#!':
        return line[:2]
    else:
        return False

def isDirective(line):
    "as for isBlockDelimiter"
    return directive(line) != False

def isNonTrivial(lines):
    """Returns True if not all lines are simply whitespace.
    This is to avoid compile() throwing a syntax error"""
    for line in lines:
        if line.strip() != '':
            return True
    return False

def location(handle, title, lineno=None):
    if lineno is None:
        lineno = handle.line
    return "file %s, test '%s' at line %s" % (handle.name(), title, str(lineno))

def reportError(location):
    raise TestFailed(location)

def reportWarning(mes, location):
    #TODO: std out
    print "\tWarning @ %s:\n\t\t%s" % (location, mes)

def slurpToBlockStart(handle):
    lines = []
    line = handle.readline()
    while not (handle.EOF or isBlockDelimiter(line)):
        lines.append(line)
        line = handle.readline()
    title = blockDelimiter(line)
    return (title, lines)


def testBlock(handle, environment, atNewBlock=False):
    """Test the next block in the file opened by handle. Scans to the next block
    except if atNewBlock is not False. atNewBlock should be used if the current
    block delimiter has already been read. In this case, atNewBlock should be
    the title of the current block.
    environment is a "local" copy of globals()
    """
    # Find the ### line
    if atNewBlock==False:
        (title, lines) = slurpToBlockStart(handle)
        if lines:
            reportWarning("Skipped %s lines since no block was started before here" % len(lines), location(handle, title))
    else:
        title = atNewBlock if atNewBlock else ""

    # Check if next line is directive
    lineno_before = handle.line
    line_before = handle.readline()
    dire = directive(line_before)
    if dire == 'notest':
        # Don't test this block
        nextTitle,_ = slurpToBlockStart(handle)
        return testBlock(handle, atNewBlock=nextTitle)
    elif dire:
        reportWarning("Unknown directive: " + dire, location(handle, title))

    # Slurp the block
    if isBlockDelimiter(line_before):
        next_title = blockDelimiter(line_before)
        lines = []
    else:
        next_title, lines_after = slurpToBlockStart(handle)
        lines = [line_before] + lines_after

    try:
        if isNonTrivial(lines):
            sage_code = "".join(lines)
            py_code = preparser.preparse(sage_code)
            C = compile(py_code, handle.name(), "exec")
            #Note: It is slightly sick here
            eval(C, environment, environment)
    except Exception, e:
        # print "Eval:\n'''" + py_code + "'''"
        reportError(location(handle, title, lineno_before))

    return next_title



def testFile(filename):
    h = MyFileHandle(filename,'r')
    env = globals() #Fresh copy of globals. It will be polluted by the tests
    next_title = testBlock(h, env)
    while not h.EOF:
        next_title = testBlock(h, env, atNewBlock=next_title)



import glob
files = glob.glob("experiments/test*.sheet")
#files = glob.glob("experiments/test_grs.sheet")
for filename in files:
    print "Testing %s" % filename
    try:
        testFile(filename)
    except TestFailed, e:
        #TODO stderr
        print "\tTEST FAILED (remaining tests in file skipped):\n\tat %s\n%s" % (e.location, e.tb)
