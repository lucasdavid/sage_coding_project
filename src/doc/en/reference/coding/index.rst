.. _sage.coding:

Coding Theory
=============

Abstract classes, catalogs and databases
----------------------------------------

.. toctree::
   :maxdepth: 2

   sage/coding/decoder
   sage/coding/encoder
   sage/coding/bounds_catalog
   sage/coding/channels_catalog
   sage/coding/codes_catalog
   sage/coding/decoders_catalog
   sage/coding/encoders_catalog
   sage/coding/two_weight_db

Linear codes and related constructions
---------------------------------------

.. toctree::
   :maxdepth: 1

   sage/coding/binary_code
   sage/coding/grs
<<<<<<< HEAD
   sage/coding/guruswami_sudan/gs_decoder
   sage/coding/guruswami_sudan/interpolation
   sage/coding/guruswami_sudan/rootfinding
   sage/coding/guruswami_sudan/utils
=======
   sage/coding/hamming_code
>>>>>>> subfield_subcode
   sage/coding/linear_code
   sage/coding/punctured_code
   sage/coding/shortened_code
   sage/coding/extended_code
   sage/coding/bch
   sage/coding/grs
   sage/coding/cyclic_code
   sage/coding/concatenated_code
   sage/coding/hamming_code
   sage/coding/code_constructions
<<<<<<< HEAD
   sage/coding/punctured_code
=======
   sage/coding/subfield_subcode
>>>>>>> subfield_subcode
   sage/coding/sd_codes
   sage/coding/guava

Bounds on codes
---------------

.. toctree::
   :maxdepth: 1

   sage/coding/code_bounds
   sage/coding/delsarte_bounds

Channels and related constructions
----------------------------------

.. toctree::
   :maxdepth: 2

   sage/coding/channel_constructions

Source coding
-------------

.. toctree::
   :maxdepth: 1

Canonical forms
---------------

.. toctree::
   :maxdepth: 1

   sage/coding/codecan/codecan
   sage/coding/codecan/autgroup_can_label

.. include:: ../footer.txt

Other tools
-----------

.. toctree::
   :maxdepth: 1

   sage/coding/field_embedding
