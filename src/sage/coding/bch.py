r"""
BCH Code

Let `F = GF(q)` and `\Phi` be the splitting field of
`x^{n} - 1` over`F`, with `n` a positive integer.
Let `\alpha` be an element of multiplicative order `n` in `\Phi`.

A BCH code consists of all codewords `c(x) \in F_{n}[x]` such that
`c(\alpha^{a}) = 0`, for `a = b, b + l, b + 2\times l, \dots, b + (\delta - 2) \times l`,
with `b`, `\delta` integers such that `b < \delta` and `0 < \delta \leq n`.
"""

#*****************************************************************************
#       Copyright (C) 2015 David Lucas <david.lucas@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from linear_code import AbstractLinearCode
from cyclic_code import CyclicCode
from grs import GeneralizedReedSolomonCode
from encoder import Encoder
from decoder import Decoder, DecodingError
from sage.modules.free_module_element import vector
from sage.misc.misc_c import prod

class BCHCode(CyclicCode):
    r"""
    Representation of a BCH code as a cyclic code.

    INPUT:

    - ``F`` -- the base field for this code

    - ``n`` -- the length of the code

    - ``b`` -- the starting point for the elements in the defining set

    - ``delta`` -- the ending point for the elements in the defining set

    - ``l`` -- (default: ``1``) the jump size between two elements of the defining set

    EXAMPLES::

        sage: C = codes.BCHCode(GF(2), 15, 1, 7)
        sage: C
        [15, 5] BCH Code over Finite Field of size 2 with x^10 + x^8 + x^5 + x^4 + x^2 + x + 1 as generator polynomial

        sage: C = codes.BCHCode(GF(2), 15, 1, 4, 3)
        sage: C
        [15, 7] BCH Code over Finite Field of size 2 with x^8 + x^7 + x^5 + x^4 + x^3 + x + 1
        as generator polynomial
    """

    def __init__(self, F, n, b, delta, l = 1):

        if not (delta <= n and delta > 1):
            raise ValueError("delta must belong to [2, n]")

        D = []
        d = b
        for i in range(0, delta - 1):
            D.append(d)
            d = (d + l) % n

        try:
            super(BCHCode, self).__init__(field = F, length = n, D = D)
        except ValueError, e:
            raise e
        self._default_decoder_name = "UnderlyingGRS"
        self._jump_size = l
        self._starting_point = b
        self._delta = delta

    def __eq__(self, other):
        r"""
        Tests equality between BCH Code objects.

        EXAMPLES::

            sage: F = GF(16, 'a')
            sage: n = 15
            sage: C1 = codes.BCHCode(F, n, 1, 2)
            sage: C2 = codes.BCHCode(F, n, 1, 2)
            sage: C1 == C2
            True
        """
        return isinstance(other, BCHCode) \
                and self.length() == other.length() \
                and self.jump_size() == other.jump_size() \
                and self.starting_point() == other.starting_point() \

    def __ne__(self, other):
        r"""
        Tests inequality of BCH Code objects.

        EXAMPLES::

            sage: F = GF(16, 'a')
            sage: n = 15
            sage: C1 = codes.BCHCode(F, 15, 1, 2)
            sage: C2 = codes.BCHCode(F, 13, 1, 2)
            sage: C1 != C2
            True
        """
        return not self.__eq__(other)

    def _repr_(self):
        r"""
        Returns a string representation of ``self``.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(2), 15, 1, 7)
            sage: C
            [15, 5] BCH Code over Finite Field of size 2 with x^10 + x^8 + x^5 + x^4 + x^2 + x + 1 as generator polynomial
        """
        return "[%s, %s] BCH Code over %s with %s as generator polynomial"\
                % (self.length(), self.dimension(),\
                self.base_field(), self.generator_polynomial())

    def _latex_(self):
        r"""
        Returns a latex representation of ``self``.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(2), 15, 1, 7)
            sage: latex(C)
            [15, 5] \textnormal{ BCH Code over } \Bold{F}_{2} \textnormal{ with } x^{10} + x^{8} + x^{5} + x^{4} + x^{2} + x + 1 \textnormal{ as generator polynomial}
        """
        return "[%s, %s] \\textnormal{ BCH Code over } %s \\textnormal{ with } %s \\textnormal{ as generator polynomial}"\
                % (self.length(), self.dimension(),\
                self.base_field()._latex_(), self.generator_polynomial()._latex_())

    def jump_size(self):
        r"""
        Returns the jump size between two consecutive elements of the defining set of ``self``.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(2), 15, 1, 4, 2)
            sage: C.jump_size()
            2
        """
        return self._jump_size

    def starting_point(self):
        return self._starting_point


    def bch_to_grs(self):
        r"""
        Returns the underlying GRS code from which ``self`` was derived.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(2), 15, 1, 2, 2)
            sage: C.bch_to_grs()
            [15, 13, 3] Generalized Reed-Solomon Code over Finite Field in b of size 2^4
        """
        l = self.jump_size()
        alpha = self.root_of_unity()
        b = self.starting_point()
        n = self.length()

        grs_dim = n - self.bch_bound(arithmetic = True) + 1
        evals = []
        pcm = []
        for i in range(1, n + 1):
            evals.append(alpha ** (l * (i - 1)))
            pcm.append(alpha ** (b * (i - 1)))


        multipliers_product = [1/prod([evals[i] - evals[h] for h in range(len(evals)) if h != i])
                    for i in range(len(evals))]
        column_multipliers = [multipliers_product[i]/pcm[i] for i in range(n)]

        return GeneralizedReedSolomonCode(evals, grs_dim, column_multipliers)










class BCHUnderlyingGRSDecoder(Decoder):
    r"""
    A decoder which decodes through the underlying
    :class:`sage.coding.grs.GeneralizedReedSolomonCode` code of the provided BCH code.

    """

    def __init__(self, code, grs_decoder = "KeyEquationSyndrome"):
        r"""

        EXAMPLES::

            sage: C = codes.BCHCode(GF(4, 'a'), 15, 1, 3, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: D
            Decoder through the underlying GRS code of [15, 11] BCH Code over Finite Field in a of size 2^2 with x^4 + a*x^3 + a as generator polynomial
        """

        self._grs_code = code.bch_to_grs()
        self._grs_decoder = self._grs_code.decoder(grs_decoder)
        self._decoder_type = self._grs_decoder.decoder_type()
        super(BCHUnderlyingGRSDecoder, self).__init__(code, code.ambient_space(), "Vector")

    def _repr_(self):
        r"""
        Returns a string representation of ``self``.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(4, 'a'), 15, 1, 3, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: D
            Decoder through the underlying GRS code of [15, 11] BCH Code over Finite Field in a of size 2^2 with x^4 + a*x^3 + a as generator polynomial
        """
        return "Decoder through the underlying GRS code of %s" % self.code()

    def _latex_(self):
        r"""
        Returns a latex representation of ``self``.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(4, 'a'), 15, 1, 3, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: latex(D)
            \textnormal{Decoder through the underlying GRS code of } [15, 11] \textnormal{ BCH Code over } \Bold{F}_{2^{2}} \textnormal{ with } x^{4} + a x^{3} + a \textnormal{ as generator polynomial}
        """
        return "\\textnormal{Decoder through the underlying GRS code of } %s" % (self.code()._latex_())

    def grs_code(self):
        r"""
        Returns the underlying GRS code of :meth:`sage.coding.decoder.Decoder.code`.


        EXAMPLES::

            sage: C = codes.BCHCode(GF(2), 15, 1, 2, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: D.grs_code()
            [15, 13, 3] Generalized Reed-Solomon Code over Finite Field in b of size 2^4
        """
        return self._grs_code

    def grs_decoder(self):
        r"""
        Returns the decoder used to decode words of :meth:`grs_code`.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(4, 'a'), 15, 1, 3, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: D.grs_decoder()
            Key equation decoder for [15, 13, 3] Generalized Reed-Solomon Code over Finite Field in b of size 2^4
        """
        return self._grs_decoder

    def bch_word_to_grs(self, c):
        r"""
        Returns ``c`` converted as a codeword of :meth:`grs_code`.

        EXAMPLES::

            sage: F = GF(4, 'a')
            sage: a = F.gen()
            sage: C = codes.BCHCode(GF(4, 'a'), 15, 1, 3, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: c = vector(F, [0, a, 1, a, 0, 1, 1, 1, a, 0, 0, a + 1, a, 0, 1])
            sage: D.bch_word_to_grs(c)
            (0, b^2 + b, 1, b^2 + b, 0, 1, 1, 1, b^2 + b, 0, 0, b^2 + b + 1, b^2 + b, 0, 1)
        """
        mapping = self.code().splitting_field_material()[1]
        a = [mapping(i) for i in c]

        return vector(a)

    def grs_word_to_bch(self, c):
        r"""
        Returns ``c`` converted as a codeword of :meth:`sage.coding.decoder.Decoder.code`.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(4, 'a'), 15, 1, 3, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: Cgrs = D.grs_code()
            sage: Fgrs = Cgrs.base_field()
            sage: b = Fgrs.gen()
            sage: c = vector(Fgrs, [0, b^2 + b, 1, b^2 + b, 0, 1, 1, 1, b^2 + b, 0, 0, b^2 + b + 1, b^2 + b, 0, 1])
            sage: D.grs_word_to_bch(c)
            (0, a, 1, a, 0, 1, 1, 1, a, 0, 0, a + 1, a, 0, 1)
        """
        C = self.code()
        mat = C.splitting_field_material()
        mapping, alpha, h = mat[0], mat[2], mat[3]
        a = []
        for i in c:
            if i != 0:
                a.append(alpha ** (h * mapping[i]))
            else:
                a.append(0)
        return vector(a)

    def decode_to_code(self, y):
        r"""
        Decodes ``y`` to an element in :meth:`sage.coding.decoder.Decoder.code`.

        EXAMPLES::

            sage: F = GF(4, 'a')
            sage: a = F.gen()
            sage: C = codes.BCHCode(F, 15, 1, 3, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: y = vector(F, [a, a + 1, 1, a + 1, 1, a, a + 1, a + 1, 0, 1, a + 1, 1, 1, 1, a])
            sage: D.decode_to_code(y)
            (a, a + 1, 1, a + 1, 1, a, a + 1, a + 1, 0, 1, a + 1, 1, 1, 1, a)
            sage: D.decode_to_code(y) in C
            True
        """
        ygrs = self.bch_word_to_grs(y)
        try:
            cgrs = self.grs_decoder().decode_to_code(ygrs)
        except DecodingError, e:
            raise e
        return self.grs_word_to_bch(cgrs)

    def decoding_radius(self):
        r"""
        Returns maximal number of errors that ``self`` can decode.

        EXAMPLES::

            sage: C = codes.BCHCode(GF(4, 'a'), 15, 1, 3, 2)
            sage: D = codes.decoders.BCHUnderlyingGRSDecoder(C)
            sage: D.decoding_radius()
            1
        """
        return (self.code().bch_bound(arithmetic = True) - 1) // 2

####################### registration ###############################

BCHCode._registered_decoders["UnderlyingGRS"] = BCHUnderlyingGRSDecoder
BCHUnderlyingGRSDecoder._decoder_type = {"dynamic"}
