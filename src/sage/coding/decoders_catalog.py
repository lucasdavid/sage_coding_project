r"""
Index of decoders

The ``codes.decoders`` object may be used to access the decoders that Sage can build.

**Generic decoders**

- :func:`linear_code.LinearCodeSyndromeDecoder <sage.coding.linear_code.LinearCodeSyndromeDecoder>`
- :func:`linear_code.LinearCodeNearestNeighborDecoder <sage.coding.linear_code.LinearCodeNearestNeighborDecoder>`
- :func:`linear_code.LinearCodeTrivialDecoder <sage.coding.linear_code.LinearCodeTrivialDecoder>`

**Subfield subcode decoder**

- :class:`subfield_subcode.SubfieldSubcodeOriginalCodeDecoder <sage.coding.subfield_subcode.SubfieldSubcodeOriginalCodeDecoder>`

**Generalized Reed-Solomon code decoders**

- :func:`grs.GRSBerlekampWelchDecoder <sage.coding.grs.GRSBerlekampWelchDecoder>`
- :func:`grs.GRSErrorErasureDecoder <sage.coding.grs.GRSErrorErasureDecoder>`
- :func:`grs.GRSGaoDecoder <sage.coding.grs.GRSGaoDecoder>`
- :func:`grs.GRSKeyEquationSyndromeDecoder <sage.coding.grs.GRSKeyEquationDecoder>`

**Concatenated code decoders**

- :func:`concatenated_code.ConcatenatedCodeInnerAndOuterDecodingDecoder <sage.coding.concatenated_code.ConcatenatedCodeInnerAndOuterDecodingDecoder>`

**Cyclic code decoders**

- :func:`cyclic_code.CyclicCodeSurroundingBCHDecoder <sage.coding.cyclic_code.CyclicCodeSurroundingBCHDecoder>`

**BCH code decoders**

- :func:`bch.BCHUnderlyingGRSDecoder <sage.coding.bch.BCHUnderlyingGRSDecoder>`

**Punctured code decoders**

- :func:`punctured_code.PuncturedCodeOriginalCodeDecoder <sage.coding.punctured_code.PuncturedCodeOriginalCodeDecoder>`

**Shortened code decoders**

- :func:`shortened_code.ShortenedCodeOriginalCodeDecoder <sage.coding.shortened_code.ShortenedCodeOriginalCodeDecoder>`

**Extended code decoders**

- :func:`extended_code.ExtendedCodeOriginalCodeDecoder <sage.coding.extended_code.ExtendedCodeOriginalCodeDecoder>`

**Generalized Reed-Solomon code decoders**

- :func:`guruswami_sudan.gs_decoder.GRSGuruswamiSudanDecoder <sage.coding.guruswami_sudan.gs_decoder.GRSGuruswamiSudanDecoder>`

.. NOTE::

    To import these names into the global namespace, use:

        sage: from sage.coding.decoders_catalog import *
"""
#*****************************************************************************
#       Copyright (C) 2009 David Joyner <wdjoyner@gmail.com>
#                     2015 David Lucas <david.lucas@inria.fr>
#
#  Distributed under the terms of the GNU General Public License (GPL),
#  version 2 or later (at your preference).
#
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from concatenated_code import ConcatenatedCodeInnerAndOuterDecodingDecoder
from cyclic_code import CyclicCodeSurroundingBCHDecoder
from bch import BCHUnderlyingGRSDecoder
from punctured_code import PuncturedCodeOriginalCodeDecoder
from shortened_code import ShortenedCodeOriginalCodeDecoder
from extended_code import ExtendedCodeOriginalCodeDecoder
from guruswami_sudan.gs_decoder import GRSGuruswamiSudanDecoder
from punctured_code import PuncturedCodeOriginalCodeDecoder
from linear_code import (LinearCodeSyndromeDecoder,
                         LinearCodeNearestNeighborDecoder,
                         LinearCodeTrivialDecoder)
from punctured_code import PuncturedCodeOriginalCodeDecoder
from subfield_subcode import SubfieldSubcodeOriginalCodeDecoder
from grs import (GRSBerlekampWelchDecoder,
                 GRSGaoDecoder,
                 GRSKeyEquationSyndromeDecoder,
                 GRSErrorErasureDecoder)
