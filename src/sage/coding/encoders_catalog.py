r"""
Index of encoders

The ``codes.encoders`` object may be used to access the encoders that Sage can build.

**Generic encoders**

- :func:`linear_code.LinearCodeGeneratorMatrixEncoder <sage.coding.linear_code.LinearCodeGeneratorMatrixEncoder>`
- :func:`linear_code.LinearCodeParityCheckEncoder <sage.coding.linear_code.LinearCodeParityCheckEncoder>`

**Generalized Reed-Solomon code encoders**

- :func:`grs.GRSEvaluationVectorEncoder <sage.coding.grs.GRSEvaluationVectorEncoder>`
- :func:`grs.GRSEvaluationPolynomialEncoder <sage.coding.grs.GRSEvaluationPolynomialEncoder>`

**Concatenated code encoders**

- :func:`concatenated_code.ConcatenatedCodeOuterFieldEncoder <sage.coding.concatenated_code.ConcatenatedCodeOuterFieldEncoder>`
- :func:`concatenated_code.ConcatenatedCodeGeneratorMatrixEncoder <sage.coding.concatenated_code.ConcatenatedCodeGeneratorMatrixEncoder>`

**Cyclic code encoders**

- :func:`cyclic_code.CyclicCodePolynomialEncoder <sage.coding.cyclic_code.CyclicCodePolynomialEncoder>`
- :func:`cyclic_code.CyclicCodeVectorEncoder <sage.coding.cyclic_code.CyclicCodeVectorEncoder>`

**Punctured code encoders**

- :func:`punctured_code.PuncturedCodePuncturedMatrixEncoder <sage.coding.punctured_code.PuncturedCodePuncturedMatrixEncoder>`

**Shortened code encoders**

- :func:`shortened_code.ShortenedCodeShortenedMatrixEncoder <sage.coding.shortened_code.ShortenedCodeShortenedMatrixEncoder>`

**Extended code encoders**

- :func:`extended_code.ExtendedCodeExtendedMatrixEncoder <sage.coding.extended_code.ExtendedCodeExtendedMatrixEncoder>`

**Generalized Reed-Solomon code encoders**

- :class:`grs.GRSEvaluationVectorEncoder <sage.coding.grs.GRSEvaluationVectorEncoder>`
- :class:`grs.GRSEvaluationPolynomialEncoder <sage.coding.grs.GRSEvaluationPolynomialEncoder>`

.. NOTE::

    To import these names into the global namespace, use:

        sage: from sage.coding.encoders_catalog import *
"""
#*****************************************************************************
#       Copyright (C) 2009 David Joyner <wdjoyner@gmail.com>
#                     2015 David Lucas <david.lucas@inria.fr>
#
#  Distributed under the terms of the GNU General Public License (GPL),
#  version 2 or later (at your preference).
#
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from sage.misc.lazy_import import lazy_import as _lazy_import
_lazy_import('sage.coding.linear_code', ['LinearCodeGeneratorMatrixEncoder',
                                         'LinearCodeParityCheckEncoder'])
_lazy_import('sage.coding.grs', ['GRSEvaluationVectorEncoder',
                                 'GRSEvaluationPolynomialEncoder'])
_lazy_import('sage.coding.concatenated_code', ['ConcatenatedCodeOuterFieldEncoder',
                                               'ConcatenatedCodeGeneratorMatrixEncoder'])
_lazy_import('sage.coding.cyclic_code', ['CyclicCodePolynomialEncoder',
                                         'CyclicCodeVectorEncoder'])
_lazy_import('sage.coding.punctured_code', 'PuncturedCodePuncturedMatrixEncoder')
_lazy_import('sage.coding.shortened_code', 'ShortenedCodeShortenedMatrixEncoder')
_lazy_import('sage.coding.extended_code', 'ExtendedCodeExtendedMatrixEncoder')
_lazy_import('sage.coding.grs', ['GRSEvaluationVectorEncoder', 'GRSEvaluationPolynomialEncoder'])
_lazy_import('sage.coding.subfield_subcode', 'SubfieldSubcodeParityCheckEncoder')
