r"""
Shortened code

Let `C` be a linear code. Let `C_i` be the set of all words of `C` with the `i`-th coordinate being zero.
Let `C_{s_{i}}` be the set of all words of `C_i` punctured in the position i. `C_{s_{i}}` is the
shortened code of `C` on i.
"""

#*****************************************************************************
#       Copyright (C) 2015 David Lucas <david.lucas@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from linear_code import (AbstractLinearCode,\
        LinearCodeGeneratorMatrixEncoder,\
        LinearCodeSyndromeDecoder,\
        LinearCodeNearestNeighborDecoder,\
        LinearCodeTrivialDecoder)
from encoder import Encoder
from decoder import Decoder
from sage.rings.integer import Integer
from sage.misc.cachefunc import cached_method
from copy import copy
from sage.modules.free_module_element import vector

class ShortenedCode(AbstractLinearCode):
    r"""
    Representation of a shortened code.

    INPUT:

    - ``C`` - A linear code

    - ``positions`` -- a list of positions where ``C`` will be shortened

    EXAMPLES::

        sage: C = codes.RandomLinearCode(11, 5, GF(7))
        sage: Cp = codes.ShortenedCode(C, 3)
        sage: Cp
        Shortened code coming from Linear code of length 11, dimension 5 over Finite Field of size 7 shortened on positions [3]

        sage: Cp = codes.ShortenedCode(C, [3, 5])
        sage: Cp
        Shortened code coming from Linear code of length 11, dimension 5 over Finite Field of size 7 shortened on positions [3, 5]
    """

    _registered_encoders = {}
    _registered_decoders = {}

    def __init__(self, C, positions):
        r"""
        TESTS::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, 13)
            Traceback (most recent call last):
            ...
            ValueError: Positions to shorten must be positive integers smaller than the length of the provided code
        """
        if not isinstance(positions, (Integer, int, tuple, list)):
            raise TypeError("positions must be either a Sage Integer, a Python int, a tuple or a list")
        if isinstance(positions, (Integer, int)):
            positions = [positions]
        if not isinstance(C, AbstractLinearCode):
            raise ValueError("Provided code must be a linear code")
        if not all (i in range(C.length()) for i in positions):
            raise ValueError("Positions to shorten must be positive integers smaller than the length of the provided code")
        unique_positions = set()
        for i in positions:
            unique_positions.add(i)
        for i in unique_positions:
            positions = []
        for i in unique_positions:
            positions.append(i)
        super(ShortenedCode, self).__init__(C.base_ring(),  C.length() - len(positions),\
            "ShortenedMatrix", "OriginalCode")
        positions.sort()
        self._original_code = C
        self._positions = positions

    def __eq__(self, other):
        r"""
        Tests equality between two Shortened codes.

        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp1 = codes.ShortenedCode(C, [3,5])
            sage: Cp2 = codes.ShortenedCode(C, [3,5])
            sage: Cp1 == Cp2
            True
        """
        return isinstance(other, ShortenedCode) \
                and self.shortened_positions() == other.shortened_positions() \
                and self.original_code() == other.original_code()

    def __ne__(self, other):
        r"""
        Tests inequality between two Shortened codes.

        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp1 = codes.ShortenedCode(C, 2)
            sage: Cp2 = codes.ShortenedCode(C, 3)
            sage: Cp1 != Cp2
            True
        """
        return not self.__eq__(other)

    def _repr_(self):
        r"""
        Returns a string representation of ``self``.

        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, [3, 5])
            sage: Cp
            Shortened code coming from Linear code of length 11, dimension 5 over Finite Field of size 7 shortened on positions [3, 5]
        """
        return "Shortened code coming from %s shortened on positions %s"\
                % (self.original_code(), self.shortened_positions())

    def _latex_(self):
        r"""
        Returns a latex representation of ``self``.

        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, [3, 5])
            sage: latex(Cp)
            \textnormal{Shortened code coming from Linear code of length 11, dimension 5 over Finite Field of size 7 shortened on positions } [3, 5]
        """
        return "\\textnormal{Shortened code coming from %s shortened on positions } %s"\
                % (self.original_code(), self.shortened_positions())

    def shortened_positions(self):
        r"""
        Returns the list of positions which were shortened on the original code.

        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, [3,5])
            sage: Cp.shortened_positions()
            [3, 5]
        """
        return self._positions

    def original_code(self):
        r"""
        Returns the linear code which was shortened to get ``self``.

        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, [3,5])
            sage: Cp.original_code()
            Linear code of length 11, dimension 5 over Finite Field of size 7
        """
        return self._original_code

    @cached_method
    def dimension(self):
        r"""
        Returns the dimension of ``self``.

        EXAMPLES::

            sage: set_random_seed(42)
            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, 3)
            sage: Cp.dimension()
            4
        """
        return self.original_code().dimension() - len(self.shortened_positions())










class ShortenedCodeShortenedMatrixEncoder(Encoder):
    r"""
    Encoder using original code's parity check matrix to compute the shortened code's
    generator matrix.

    INPUT:

    - ``code`` -- The associated code of this encoder.
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, 3)
            sage: E = codes.encoders.ShortenedCodeShortenedMatrixEncoder(Cp)
            sage: E
            Shortened matrix-based encoder for Shortened code coming from Linear code of length 11, dimension 5 over Finite Field of size 7 shortened on positions [3]
        """
        super(ShortenedCodeShortenedMatrixEncoder, self).__init__(code)

    def _repr_(self):
        r"""
        Returns a string representation of ``self``.

        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, 3)
            sage: E = codes.encoders.ShortenedCodeShortenedMatrixEncoder(Cp)
            sage: E
            Shortened matrix-based encoder for Shortened code coming from Linear code of length 11, dimension 5 over Finite Field of size 7 shortened on positions [3]
        """
        return "Shortened matrix-based encoder for %s" % self.code()

    def _latex_(self):
        r"""
        Returns a latex representation of ``self``.

        EXAMPLES::

            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, 3)
            sage: E = codes.encoders.ShortenedCodeShortenedMatrixEncoder(Cp)
            sage: latex(E)
            \textnormal{Shortened matrix-based encoder for }\textnormal{Shortened code coming from Linear code of length 11, dimension 5 over Finite Field of size 7 shortened on positions } [3]
        """
        return "\\textnormal{Shortened matrix-based encoder for }%s" % self.code()._latex_()

    @cached_method
    def generator_matrix(self):
        r"""
        Returns a generator matrix of the associated code of ``self``.

        EXAMPLES::

            sage: set_random_seed(10)
            sage: C = codes.RandomLinearCode(11, 5, GF(7))
            sage: Cp = codes.ShortenedCode(C, 3)
            sage: E = codes.encoders.ShortenedCodeShortenedMatrixEncoder(Cp)
            sage: E.generator_matrix()
            [1 0 0 0 0 5 2 6 0 6]
            [0 1 0 0 1 5 3 5 5 4]
            [0 0 1 0 4 6 6 2 2 2]
            [0 0 0 1 6 0 5 0 6 0]
        """
        C = self.code().original_code()
        pos = self.code().shortened_positions()
        H = C.parity_check_matrix()
        H_reduced = H.delete_columns(pos)
        H_reduced = H_reduced.echelon_form()
        delete = []
        cpt = 0
        for i in H_reduced.rows():
            if i.is_zero():
                delete.append(cpt)
            cpt += 1
        G = H_reduced.right_kernel()
        return G.basis_matrix()









class ShortenedCodeOriginalCodeDecoder(Decoder):
    r"""
    Decoder decoding through a decoder over the original code of its shortened code.

    INPUT:

    - ``code`` -- The associated code of this decoder

    - ``original_decoder`` -- (default: ``None``) the decoder that will be used over the original code.
      It has to be a decoder object over the original code.
      If ``original_decoder`` is set to ``None``, it will use the default decoder of the original code.

    - ``**kwargs`` -- all extra arguments are forwarded to original code's decoder

    EXAMPLES::

        sage: C = codes.GeneralizedReedSolomonCode(GF(16, 'a').list()[:15], 7)
        sage: Cs = codes.ShortenedCode(C, 3)
        sage: D = codes.decoders.ShortenedCodeOriginalCodeDecoder(Cs)
        sage: D
        Decoder of Shortened code coming from [15, 7, 9] Generalized Reed-Solomon Code over Finite Field in a of size 2^4 shortened on positions [3] through Gao decoder for [15, 7, 9] Generalized Reed-Solomon Code over Finite Field in a of size 2^4
    """

    def __init__(self, code, original_decoder = None, **kwargs):
        r"""
        TESTS:

        If one tries to use a decoder whose code is not the original code, it returns an error::

            sage: C1 = codes.GeneralizedReedSolomonCode(GF(16, 'a').list()[:15], 7)
            sage: Cs = codes.ShortenedCode(C1, 3)
            sage: C2 = codes.GeneralizedReedSolomonCode(GF(13).list()[:12], 7)
            sage: Dc2 = C2.decoder()
            sage: D = codes.decoders.ShortenedCodeOriginalCodeDecoder(Cs, original_decoder = Dc2)
            Traceback (most recent call last):
            ...
            ValueError: Original decoder must have the original code as associated code
        """
        original_code = code.original_code()
        if original_decoder is not None and not original_decoder.code() == original_code:
            raise ValueError("Original decoder must have the original code as associated code")
        elif original_decoder is None:
            self._original_decoder = original_code.decoder()
        else:
            self._original_decoder = original_decoder
        self._decoder_type = copy(self._decoder_type)
        self._decoder_type.remove("dynamic")
        self._decoder_type = self._original_decoder.decoder_type()
        super(ShortenedCodeOriginalCodeDecoder, self).__init__(code, code.ambient_space(),\
                self._original_decoder.connected_encoder())

    def _repr_(self):
        r"""
        Returns a string representation of ``self``.

        EXAMPLES::

            sage: C = codes.GeneralizedReedSolomonCode(GF(16, 'a').list()[:15], 7)
            sage: Cs = codes.ShortenedCode(C, 3)
            sage: D = codes.decoders.ShortenedCodeOriginalCodeDecoder(Cs)
            sage: D
            Decoder of Shortened code coming from [15, 7, 9] Generalized Reed-Solomon Code over Finite Field in a of size 2^4 shortened on positions [3] through Gao decoder for [15, 7, 9] Generalized Reed-Solomon Code over Finite Field in a of size 2^4
        """
        return "Decoder of %s through %s" % (self.code(), self.original_decoder())


    def _latex_(self):
        r"""
        Returns a latex representation of ``self``.

        EXAMPLES::

            sage: C = codes.GeneralizedReedSolomonCode(GF(16, 'a').list()[:15], 7)
            sage: Cs = codes.ShortenedCode(C, 3)
            sage: D = codes.decoders.ShortenedCodeOriginalCodeDecoder(Cs)
            sage: latex(D)
            \textnormal{Decoder of } Shortened code coming from [15, 7, 9] Generalized Reed-Solomon Code over Finite Field in a of size 2^4 shortened on positions [3] \textnormal{ through } Gao decoder for [15, 7, 9] Generalized Reed-Solomon Code over Finite Field in a of size 2^4
        """
        return "\\textnormal{Decoder of } %s \\textnormal{ through } %s" % (self.code(), self.original_decoder())

    def original_decoder(self):
        r"""
        Returns the decoder over the original code that will be used to decode words of
        :meth:`sage.coding.decoder.Decoder.code`.

        EXAMPLES::

            sage: C = codes.GeneralizedReedSolomonCode(GF(16, 'a').list()[:15], 7)
            sage: Cs = codes.ShortenedCode(C, 3)
            sage: D = codes.decoders.ShortenedCodeOriginalCodeDecoder(Cs)
            sage: D.original_decoder()
            Gao decoder for [15, 7, 9] Generalized Reed-Solomon Code over Finite Field in a of size 2^4
        """
        return self._original_decoder

    def decode_to_code(self, y):
        r"""
        Decodes ``y`` to an element in :meth:`sage.coding.decoder.Decoder.code`.

        EXAMPLES::

            sage: C = codes.GeneralizedReedSolomonCode(GF(16, 'a').list()[:15], 7)
            sage: Cs = codes.ShortenedCode(C, 3)
            sage: D = codes.decoders.ShortenedCodeOriginalCodeDecoder(Cs)
            sage: c = Cs.random_element()
            sage: Chan = channels.StaticErrorRateChannel(Cs.ambient_space(), D.decoding_radius())
            sage: y = Chan(c)
            sage: y in Cs
            False
            sage: D.decode_to_code(y) == c
            True
        """
        D = self.original_decoder()
        C = self.code()
        pts = C.shortened_positions()
        F = C.base_field()
        zero = F.zero()
        if 'error-erasure' in D.decoder_type():
            if not isinstance(y, (tuple, list)):
                raise TypeError("You must provide the erasure vector")
            y_original = copy(y[0].list())
            e_original = copy(y[1].list())
            for i in pts:
                y_original.insert(i, zero)
                e_original.insert(i, zero)
            y = (vector(F, y_original), vector(F, e_original))
            c_original = list(D.decode_to_code(y))
        else:
            y_original = copy(y.list())
            for i in pts:
                y_original.insert(i, zero)
            c_original = list(D.decode_to_code(vector(F, y_original)))
        c_shortened = []
        start = 0
        for i in pts:
            c_shortened += c_original[start:i]
            start = i + 1
        c_shortened += c_original[start:len(c_original)]
        return vector(F, c_shortened)

    def decoding_radius(self, number_erasures = None):
        r"""
        Returns maximal number of errors that ``self`` can decode.

        EXAMPLES::

            sage: C = codes.GeneralizedReedSolomonCode(GF(16, 'a').list()[:15], 7)
            sage: Cs = codes.ShortenedCode(C, 3)
            sage: D = codes.decoders.ShortenedCodeOriginalCodeDecoder(Cs)
            sage: D.decoding_radius()
            4
        """
        D = self.original_decoder()
        if 'error-erasure' in D.decoder_type():
            if number_erasures is None:
                raise ValueError("Please provide the number of erasures")
            diff = self.code().original_code().minimum_distance() - number_erasures
            if diff <= 0:
                raise ValueError("The number of erasures exceed decoding capability")
            return diff // 2
        return D.decoding_radius()

####################### registration ###############################

ShortenedCode._registered_encoders["ShortenedMatrix"] = ShortenedCodeShortenedMatrixEncoder
ShortenedCode._registered_decoders["Syndrome"] = LinearCodeSyndromeDecoder
ShortenedCode._registered_decoders["NearestNeighbor"] = LinearCodeNearestNeighborDecoder
ShortenedCode._registered_decoders["Trivial"] = LinearCodeTrivialDecoder
ShortenedCode._registered_decoders["OriginalCode"] = ShortenedCodeOriginalCodeDecoder
ShortenedCodeOriginalCodeDecoder._decoder_type = {"dynamic"}
